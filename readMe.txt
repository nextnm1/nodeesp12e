This module called Node is responsible for monitoring de sensor (matrix placed) in the shelve.

INPUTS:

This Node can receive orders to be configure himself or to make some actions

Example(deprecated):
{node: '2134509702', msg: {actions: [{ name: "led", value: "off" }],configs: [{ name: "interval", value: 1000 }]}}



OUTPUTS(deprecated):

This Node will periodically send it state in terms of action a configurations and the lines pressed in a column (it will send column by colums), as you can see below:

{"sensors":[{"values":[],"name":"col3"}],"actions":[{"name":"led","value":"on"}],"configs":[{"name":"interval","value":1000}],"node":2134509702}

New version:

<Nlines>;<colId>;<lineIdPressed>;
