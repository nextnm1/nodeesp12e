//************************************************************
// this is a simple example that uses the painlessMesh library
//
// 1. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 2. prints anything it receives to Serial.print
//
//
//************************************************************
// Bread pequena ************** leitura
// Roxo GPIO4 S0
// Laranja GPIO0 S1
// Branco GPIO2 S2
// Vermelho GPIO15 S3

// Bread grande ************** alimentação

// Azul GPIO16 S0
// Laranja GPIO14 S1
// Vermelho GPIO12 S2
// Preto GPIO13 S3

#include "painlessMesh.h"
#include <set>
#include <string>
#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555


Scheduler userScheduler; // to control your personal task
painlessMesh  mesh;

int globalCol=0;
const int sizeMatrixLines = 15;// na verdade sao 50 porque 2 es tao resevados para borda extra
const int sizeMatrixColums = 15; // na verdade sao 100


const int sizeDiff=(sizeMatrixLines/2)*(sizeMatrixColums/2);
int different[sizeDiff];


// User stub
void sendMessage() ; // Prototype so PlatformIO doesn't complain

Task taskSendMessage( TASK_SECOND * 1 , TASK_FOREVER, &sendMessage );

void getBinary(int * v, int n){//6 bits
  int c,k;

  for (c = 5; c >= 0; c--)
  {
    k = n >> c;

    if (k & 1)
    v[c]=1;
    else
    v[c]=0;
  }
}

int getValueFromSensor( int index){
  int v[6];
  v[0]=0;
  v[1]=0;
  v[2]=0;
  v[3]=0;
  v[4]=0;
  v[5]=0;
  getBinary(v, index);



  if (v[0]==1) {
    digitalWrite(4, HIGH);
  }
  else{
    digitalWrite(4, LOW);
  }

  if (v[1]==1) {
    digitalWrite(0, HIGH);
  }
  else{
    digitalWrite(0, LOW);
  }

  if (v[2]==1) {
    digitalWrite(2, HIGH);
  }
  else{
    digitalWrite(2, LOW);
  }

  if (v[3]==1) {
    digitalWrite(15, HIGH);
  }
  else{
    digitalWrite(15, LOW);
  }

  int reading =analogRead(A0);
  // Serial.printf("Linha %d - %d  ",index,reading);
  // Serial.printf("Binario: %d%d%d%d\n",v[3],v[2],v[1],v[0]);
  return reading;
}

void setPowerLine( int index){
  int v[6];
  v[0]=0;
  v[1]=0;
  v[2]=0;
  v[3]=0;
  v[4]=0;
  v[5]=0;
  getBinary(v, index);



  if (v[0]==1) {
    digitalWrite(16, HIGH);
  }
  else{
    digitalWrite(16, LOW);
  }

  if (v[1]==1) {
    digitalWrite(14, HIGH);
  }
  else{
    digitalWrite(14, LOW);
  }

  if (v[2]==1) {
    digitalWrite(12, HIGH);
  }
  else{
    digitalWrite(12, LOW);
  }

  if (v[3]==1) {
    digitalWrite(13, HIGH);
  }
  else{
    digitalWrite(13, LOW);
  }

}

void sendMessage() {
  int row=0;
  char buffer [6];
  char buffer2 [6];
  itoa (globalCol,buffer,10);
  itoa (sizeMatrixLines,buffer2,10);
  std::string dadosStr="";
  dadosStr.append(buffer2).c_str();
  dadosStr.append(";");
  dadosStr.append(buffer).c_str();
  dadosStr.append(";");

  for ( row = 0; row < sizeMatrixLines; row++) {
    if(getValueFromSensor(row)>180){
      itoa (row,buffer,10);
      dadosStr.append(buffer).c_str();
      if (row!=sizeMatrixLines-1) {
        dadosStr.append(",");
      }
       // Serial.printf("matrix value: %d row %d col %d \n",getValueFromSensor(row),row,globalCol);
    }
  }
  dadosStr.append(";");
  globalCol++;
  setPowerLine(globalCol);
  // uint32_t dest= 2134509702;
  uint32_t dest= 2134511147;

  String str3 = String(dadosStr.c_str());
  Serial.println(str3);

  Serial.print(mesh.sendSingle(dest,str3));

  if(globalCol==sizeMatrixColums){
    globalCol=0;
  }
}
// Needed for painless library
void receivedCallback( uint32_t from, String &msg ) {
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
}

void newConnectionCallback(uint32_t nodeId) {
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections %s\n",mesh.subConnectionJson().c_str());
}

void nodeTimeAdjustedCallback(int32_t offset) {
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(),offset);
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);
  pinMode(0, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(15, OUTPUT);

  pinMode(16, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);

  digitalWrite(0, LOW);
  digitalWrite(4, LOW);
  digitalWrite(2, LOW);
  digitalWrite(15, LOW);

  digitalWrite(16, LOW);
  digitalWrite(14, LOW);
  digitalWrite(12, LOW);
  digitalWrite(13, LOW);

  Serial.begin(115200);
  int  interval= 1000;
  // Serial.printf("INTERVAL: %d\n", interval );
  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages
  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  userScheduler.addTask( taskSendMessage );
  //Task taskSendMessage(2000 , TASK_FOREVER, &sendMessage);
  taskSendMessage.enable();
  taskSendMessage.setInterval(interval);

}

void loop() {
  userScheduler.execute(); // it will run mesh scheduler as well
  mesh.update();
}
